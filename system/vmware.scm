(define-module (system vmware)
  #:use-module (guix gexp)
  #:use-module (guix store)

  #:use-module (gnu system)
  #:use-module (gnu system shadow)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system mapped-devices)

  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)

  #:use-module (gnu packages)
  #:use-module (gnu packages linux)

  #:use-module (gnu services)
  #:use-module (gnu services pm)
  #:use-module (gnu services base)
  #:use-module (gnu services linux)
  #:use-module (gnu services desktop)
  #:use-module (gnu services pam-mount)

  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd)

  #:use-module (system services pm)
  #:use-module (system services networking)
  #:use-module ((system desktop) #:prefix desktop:)
  #:use-module (packages linux))

(define users
  (cons*
   (user-account
    (name "ejansen")
    (group "users")
    (supplementary-groups '("wheel" "audio" "video" "docker" "lp"))
    (home-directory "/home/ejansen"))
   %base-user-accounts))

(define (btrfs-opts subvol)
  (format #f "noatime,space_cache=v2,ssd,compress=lzo,subvol=~a" subvol))

(define mapped-devices
  (list (mapped-device
         (source (uuid "b75bb152-81d7-495c-b173-a98f93e298b5"))
         (target "crypt")
         (type luks-device-mapping))))

(define file-systems
  (cons* (file-system
           (type "btrfs")
           (device (file-system-label "system"))
           (mount-point "/")
           (options (btrfs-opts "@"))
           (dependencies mapped-devices))
         (file-system
           (type "btrfs")
           (device (file-system-label "system"))
           (mount-point "/gnu")
           (options (btrfs-opts "@gnu"))
           (dependencies mapped-devices))
         (file-system
           (type "btrfs")
           (device (file-system-label "system"))
           (mount-point "/var/log")
           (options (btrfs-opts "@logs"))
           (dependencies mapped-devices))
         (file-system
           (type "btrfs")
           (device (file-system-label "system"))
           (mount-point "home")
           (options (btrfs-opts "@home"))
           (dependencies mapped-devices))
         (file-system
           (device (uuid "XXXX-XXXX" "fat32"))
           (mount-point "/boot/efi")
           (type "vfat"))
         %base-file-systems))

(define services
  (cons*
   (service kernel-module-loader-service-type '("acpi_call"))
   (service throttled-service-type)
   (service
    upower-service-type
    (upower-configuration
     (use-percentage-for-policy? #t)))
   (service
    tlp-service-type
    (tlp-configuration
     (start-charge-thresh-bat0 75)
     (stop-charge-thresh-bat0 80)
     (start-charge-thresh-bat1 75)
     (stop-charge-thresh-bat1 80)))
   (modify-services desktop:services
     (guix-service-type
      config =>
      (guix-configuration
       (inherit config)
       (substitute-urls '("https://bordeaux.guix.gnu.org"
                          "http://ci.guix.trop.in"
                          "https://substitutes.nonguix.org"))
       (authorized-keys (cons
                         (local-file "../keys/nonguix.pub")
                         %default-authorized-guix-keys))))
     (udev-service-type
      config =>
      (udev-configuration
       (inherit config)
       (udev eudev-with-hwdb))))))

(define packages
  (append
   desktop:packages
   (map specification->package
        '("btrfs-progs"
          "curl"
          "htop"
          "docker-compose"
          "intel-media-driver"
          "libva-utils"))))

(operating-system
  (inherit desktop:system)
  (initrd microcode-initrd)
  (host-name "vmware")
  (timezone "Australia/Melbourne")
  (kernel linux)
  (firmware (list ibt-hw-firmware iwlwifi-firmware))
  (mapped-devices mapped-devices)
  (file-systems file-systems)
  (users users)
  (packages packages)
  (services services))
