#!/bin/sh
# $1 = EFI [/dev/vda1]
# $2 = ROOT [dev/vda2]

# Format the EFI boot partition
mkfs.vfat -F32 -n EFI /dev/$1

# Create an encrypted partition for the btrfs partition:
cryptsetup luksFormat /dev/$2
cryptsetup luksOpen /dev/$2 crypt

# Format the encrypted root partition and label it 'ROOT':
mkfs.btrfs -L ROOT /dev/mapper/crypt

# Mount the btrfs partition to /mnt:
mount -t btrfs /dev/mapper/crypt /mnt  

# Create the btrfs subvolumes on:
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@boot
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@store
btrfs subvolume create /mnt/@data
btrfs subvolume create /mnt/@log
btrfs subvolume create /mnt/@lib
btrfs subvolume create /mnt/@guix

#Remount the root, but now the btrfs version with options
umount /mnt
mount -o subvol=@ /dev/mapper/crypt /mnt

# Create the directories
mkdir -p /mnt/boot
mkdir -p /mnt/home
mkdir -p /mnt/gnu/store
mkdir -p /mnt/data
mkdir -p /mnt/var/log
mkdir -p /mnt/var/lib
mkdir -p /mnt/var/guix

# Mount the remaining subvolumes to the directories
mount -o compress=zstd,discard,subvol=@boot /dev/mapper/crypt /mnt/boot
mount -o compress=zstd,discard,subvol=@home /dev/mapper/crypt /mnt/home
mount -o compress=zstd,discard,subvol=@store /dev/mapper/crypt /mnt/gnu/store
mount -o compress=zstd,discard,subvol=@data /dev/mapper/crypt /mnt/data
mount -o compress=zstd,discard,subvol=@log /dev/mapper/crypt /mnt/var/log
mount -o compress=zstd,discard,subvol=@lib /dev/mapper/crypt /mnt/var/lib
mount -o compress=zstd,discard,subvol=@guix /dev/mapper/crypt /mnt/var/guix

# Mount the boot partition
mkdir -p /mnt/boot/efi
mount /dev/$1 /mnt/boot/efi

# Start the copy on write service
echo "herd start cow-store /mnt"

# The partitions are now complete and ready for Guix installation
echo "Update the config.scm with the folowing boot uuid"
blkid

echo "Update the config.scm with the folowing crypt uuid"
cryptsetup luksUUID /dev/$2
