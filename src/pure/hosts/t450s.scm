(define-module (pure hosts t450s)
  #:use-module (rde features base)
  #:use-module (rde features system)
  #:use-module (rde features wm)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system mapped-devices)
  #:use-module (ice-9 match))


;;; Hardware/host specifis features

;; TODO: Switch from UUIDs to partition labels For better
;; reproducibilty and easier setup.  Grub doesn't support luks2 yet.

(define t450s-mapped-devices
  (list (mapped-device
	 (source (uuid "b75bb152-81d7-495c-b173-a98f93e298b5"))
         (target "crypt")
         (type luks-device-mapping))))

(define (btrfs-opts subvol)
  (format #f "noatime,space_cache=v2,ssd,compress=lzo,subvol=~a" subvol))

(define t450s-file-systems
  (append
   (map (match-lambda
          ((subvol . mount-point)
           (file-system
             (type "btrfs")
             (device "/dev/mapper/crypt")
             (mount-point mount-point)
             (options (format #f "subvol=~a" subvol))
             (dependencies t450s-mapped-devices))))
        '((root . "/")
          (gnu  . "/gnu")
          (home . "/home")
          (tmp  . "/tmp")
          (log  . "/var/log")))
   (list
    (file-system
      (mount-point "/boot/efi")
      (type "vfat")
      (device (uuid "0B3B-A6B4" 'fat32))))))

(define-public %t450s-features
  (list
   (feature-host-info
    #:host-name "t450s"
    ;; ls `guix build tzdata`/share/zoneinfo
    #:timezone  "Astralia/Melbourne")
   ;;; Allows to declare specific bootloader configuration,
   ;;; grub-efi-bootloader used by default
   ;; (feature-bootloader)
   (feature-file-systems
    #:mapped-devices t450s-mapped-devices
    #:file-systems   t450s-file-systems)
   (feature-kanshi
    #:extra-config
    `((profile laptop ((output eDP-1 enable)))
      (profile docked ((output eDP-1 enable)
                       (output DP-2 scale 2)))))
   (feature-hidpi)))
