(use-modules (guix channels))

(list (channel
        (name 'rde)
        (url "https://git.sr.ht/~pure/rde")
        (branch "master")
        (commit
          "292e0e4ec26e2afb66d3f9d629bc61ab396b7546")
        (introduction
          (make-channel-introduction
            "257cebd587b66e4d865b3537a9a88cccd7107c95"
            (openpgp-fingerprint
              "2841 9AC6 5038 7440 C7E9  2FFA 2208 D209 58C1 DEB0"))))
      (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit
          "fe9bcf9db24e6f7849ad870e0853c251517fd6f0")
        (introduction
          (make-channel-introduction
            "9edb3f66fd807b096b48283debdcddccfea34bad"
            (openpgp-fingerprint
              "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA")))))
